#!/bin/bash

PGDUMP=/usr/bin/pg_dumpall
DATABASE=database
BUCKET=s3://bucketname/folder/
POSTGRES=postgres/
S3CMD=/usr/bin/s3cmd
GPG=/usr/bin/gpg
DATE=`date +%F_%H_%M_%S`
TEMP_FILE=/tmp/billing_$DATE.sql

cd /tmp;
nice $PGDUMP -l $DATABASE > $TEMP_FILE;

nice $GPG -z 3 -e -r info@bolyshev.com $TEMP_FILE;

nice $S3CMD put $TEMP_FILE.gpg $BUCKET$POSTGRES > /dev/null;

rm -f $TEMP_FILE;
rm -f $TEMP_FILE.gpg;

FILES=()
FILES+=`s3cmd ls $BUCKET$POSTGRES | grep sql | awk '{print $4}'`

SORTEDFILES=( $(
    for el in "${FILES[@]}"
    do
<------>echo "$el"
    done | sort -Vr ) )

for (( i = 0 ; i < ${#SORTEDFILES[@]} ; i++ ))
do
    if [ $i -gt 47 ]
    then
        $S3CMD del ${SORTEDFILES[$i]} > /dev/null
    fi
done

exit 0
